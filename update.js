var fs = require( `fs` ),
	fileName = `theme.js`,
	exec = require( `child_process` ).exec;

fs.watchFile( fileName, function ( current, previous ) {
	exec( `node ${ fileName }`, function ( error, stdout, stderr ) {
		if ( stderr ) { console.log( `Error: ${ error }` ); return; }

		console.log( `theme.json generated –– ${ Date().toLocaleString() }` );
		console.log( stdout );
	} )
} )
