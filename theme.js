"use strict";

var fs = require( "fs" );
var _ = require( "lodash" );

var copyright = {
    "done.generic" : "Done",
    "save.filters": "Apply Filters",
    "price.filters.min": "£0",
    "price.filters.max": "£500",
    "screen_title.filters": "Filters",
    "filters.category_group": "Categories",
    "filters.size_group": "Size",
    "filters.color_group": "Color"
};

var baseUnit = 8;
var Unit = {
    sixteenth: baseUnit / 16,
    eighth: baseUnit / 8,
    base: baseUnit,
    half: baseUnit / 2,
    quarter: baseUnit / 4,
    double: baseUnit * 2,
    triple: baseUnit * 3,
    quadruple: baseUnit * 4,
    quintuple: baseUnit * 5,
    septuple: baseUnit * 7,
    sextuple: baseUnit * 6,
    octuple: baseUnit * 8,
    nonuple: baseUnit * 9,
    quinvigintuple: baseUnit * 25
};

var Radius = {
    small: Unit.quarter,
    medium: Unit.base,
    large: Unit.double
};

const Opacity = {
    none: 0,

    lowLower: 0.4,
    low: 0.8,

    mediumLower: 0.16,
    medium: 0.24,
    mediumHigher: 0.32,

    highLower: 0.40,
    high: 0.72
};

const Color = {
    primary: {
        base: "255, 0, 255, 100",
        lighter: "255, 170, 255, 100",
        darker: "170, 0, 170, 100"
    },
    secondary: {
        base: "0, 255, 255, 100",
        lighter: "170, 255, 255, 100",
        darker: "0, 170, 170, 100"
    },
    light: {
        base: "255, 255, 255, 100",
        lighter: "221, 221, 221, 100",
        darker: "255, 255, 255, 100"
    },
    dark: {
        base: "0, 0, 0, 100",
        lighter: "0, 0, 0, 100",
        darker: "17, 17, 17, 100"
    },
    success: {
        base: "0, 255, 0, 100",
        lighter: "0, 0, 0, 100",
        darker: "17, 17, 17, 100"
    },
    warning: {
        base: "15, 15, 15, 100",
        lighter: "175, 175, 175, 100",
        darker: "10, 10, 10, 100"
    },
    error: {
        base: "229, 36, 36, 100",
        lighter: "243, 55, 55, 100",
        darker: "174, 27, 27, 100"
    },
    system: {
        clear: {
            base: "0, 0, 0, 0",
            lighter: "0, 0, 0, 0",
            darker: "0, 0, 0, 0"
        },
        red: {
            base: "255, 0, 0, 100",
            lighter: "255, 101, 101, 100",
            darker: "194, 0, 0, 100"
        }
    }
};

const FontFamily = {
    header: "SF Pro Display",
    body: "SF Pro Text",
    price: "Courier"
};

const FontDecoration = {
    None: "none",
    Underline: "underline",
    Strikethrough: "strikethrough"
};

const baseFontSize = 17;

var labelFactory = function ( deviation, fontFamily, weight ) {
    return {
        fontSize: baseFontSize + (deviation || 0),
        fontFamily: fontFamily || FontFamily.body,
        fontWeight: weight || "Regular"
    };
};

var FontStyle = {
    largeTitle: labelFactory( 17, FontFamily.header ),
    title1: labelFactory( 11, FontFamily.header ),
    title2: labelFactory( 5, FontFamily.header ),
    title3: labelFactory( 3, FontFamily.header ),
    headline: labelFactory( 0, FontFamily.header, "Semibold" ),
    body: labelFactory( ),
    callout: labelFactory( 1 ),
    subhead: labelFactory( 2 ),
    footnote: labelFactory( 4 ),
    caption1: labelFactory( 5 ),
    caption2: labelFactory( 6 )
};

var colorVariationsWithTintableElement = function ( tintableElement ) {
    var output = {
        onPrimary: Color.light.base,
        onSecondary: Color.dark.base,
        onLight: Color.dark.base,
        onDark: Color.light.base,
        onSuccess: Color.light.base,
        onWarning: Color.dark.base,
        onError: Color.light.base,
        primary: Color.primary.base,
        secondary: Color.secondary.base,
        lightDarker: Color.light.base,
        darkLighter: Color.dark.lighter,
        systemRed: Color.system.red.base
    };

    output = _.mapValues( output, function ( colorValue, colorKey ) {
        return _.set( tintableElement, "tintColor", colorValue );
    });

    return output;
};

var labelAlignmentVariationsWithLabelColorVariations = function ( labelColorVariations ) {
    var labelAlignmentVariations = { left: "Left-Top" , center: "Center-Top", right: "Right-Top" };
    var alignmentVariationWithColorVariation = function ( colorVariation, alignmentValue ) {
        var alignmentVariation = _.set( colorVariation, "alignment", alignmentValue );
        return alignmentVariation;
    };

    // Go through left/center/right, editing their values
    labelAlignmentVariations = _.mapValues( labelAlignmentVariations, function ( alignmentValue, alignmentKey ) {
        // Go through onPrimary/onSecondary/etc, editing their value
        var labelColorVariationsWithAlignmentEdits = _.mapValues( labelColorVariations, function ( colorValue, colorKey, colorVariation ) {
            var labelColorVariationWithAlignmentEdits = alignmentVariationWithColorVariation( colorValue, alignmentValue );
            return labelColorVariationWithAlignmentEdits;
        } );

        return labelColorVariationsWithAlignmentEdits;
    } );

    // console.log( `labelAlignmentVariationsWithLabelColorVariations ${ JSON.stringify( labelAlignmentVariations, true, 2 ) }` );

    return labelAlignmentVariations;
};

var Icon = {
    account: colorVariationsWithTintableElement( { asset: "icon-account" } ),
    action: colorVariationsWithTintableElement( { asset: "icon-action" } ),
    activity: colorVariationsWithTintableElement( { asset: "icon-activity" } ),
    add: colorVariationsWithTintableElement( { asset: "icon-add" } ),
    bag: colorVariationsWithTintableElement( { asset: "icon-bag" } ),
    barcodeSearch: colorVariationsWithTintableElement( { asset: "icon-barcode_search" } ),
    chevronLeft: colorVariationsWithTintableElement( { asset: "icon-chevron_left" } ),
    chevronRight: colorVariationsWithTintableElement( { asset: "icon-chevron_right" } ),
    close: colorVariationsWithTintableElement( { asset: "icon-close" } ),
    collapse: colorVariationsWithTintableElement( { asset: "icon-collapse" } ),
    expand: colorVariationsWithTintableElement( { asset: "icon-expand" } ),
    favorite_filled: colorVariationsWithTintableElement( { asset: "icon-favorite_filled" } ),
    favorite_outline: colorVariationsWithTintableElement( { asset: "icon-favorite_outline" } ),
    home_branded: colorVariationsWithTintableElement( { asset: "icon-home_branded" } ),
    home_generic: colorVariationsWithTintableElement( { asset: "icon-home_generic" } ),
    more: colorVariationsWithTintableElement( { asset: "icon-more" } ),
    search: colorVariationsWithTintableElement( { asset: "icon-search" } ),
    shop: colorVariationsWithTintableElement( { asset: "icon-search" } ),
    video: colorVariationsWithTintableElement( { asset: "icon-video" } ),
    visualSearch: colorVariationsWithTintableElement( { asset: "icon-visual_search" } ),
    wishlist: colorVariationsWithTintableElement( { asset: "icon-favourite" } )
};

const Other = {
    clientLogo: {
        onLight: "poq_logo-nav_bar-black.pdf",
        onDark: "poq_logo-nav_bar-white.pdf"
    },
    moreColours: {
        onLight: "moreColours-onLight.pdf",
        onDark: "moreColours-onDark.pdf"
    }
};

const Asset = {
    icon: Icon,
    other: Other
};

var componentButtonNoBackgroundSharedStyles = {
    primary: {
        normal: {
            tintColor: Color.primary.base
        },
        highlighted: {
            tintColor: Color.primary.lighter
        },
        focused: {
            tintColor: Color.primary.lighter
        },
        selected: {
            tintColor: Color.primary.lighter
        },
        disabled: {
            tintColor: Color.dark.ligher
        }
    },
    secondary: {
        normal: {
            tintColor: Color.secondary.base
        },
        highlighted: {
            tintColor: Color.secondary.lighter
        },
        focused: {
            tintColor: Color.secondary.lighter
        },
        selected: {
            tintColor: Color.secondary.lighter
        },
        disabled: {
            tintColor: Color.dark.lighter
        }
    },
    destructive: {
        normal: {
            tintColor: Color.system.red.base
        },
        highlighted: {
            tintColor: Color.system.red.lighter
        },
        focused: {
            tintColor: Color.system.red.lighter
        },
        selected: {
            tintColor: Color.system.red.lighter
        },
        disabled: {
            tintColor: Color.dark.lighter
        }
    }
};

var Component = function () {
    this.label = this.label();
    this.separator = this.separator;
    this.promoBanner = this.promoBanner(this.label);
    this.button = this.button(this.label, Asset);
    this.navigationBar = this.navigationBar(this.label, this.separator);
    this.searchBar = this.searchBar(this.label, this.separator, this.button, Asset);
    this.segmentedControl = this.segmentedControl(this.label);
    this.toolbar = this.toolbar(this.separator);
    this.price = this.price(this.label);
    this.rangeSlider = this.rangeSlider(this.label);
    this.sortBar = this.sortBar(this.separator);
    this.tableCellAccessory = this.tableCellAccessory(this.button);
    this.tableCell = this.tableCell(this.label, this.price, this.separator, this.tableCellAccessory, this.button);
    this.collectionCell = this.collectionCell(this.button, this.promoBanner, this.price, this.label, Asset);
};

Component.prototype.label = function () {
    var output = _.mapValues( FontStyle, function ( fontStyleElement ) {
        var output = labelAlignmentVariationsWithLabelColorVariations( colorVariationsWithTintableElement( fontStyleElement ) );
        return output;
    });

    return output;
};

Component.prototype.button = function (label, asset) {
    return {
        icon: {
            onLight: {
                regular: {
                    normal: {
                        tintColor: Color.primary.base,
                        asset: asset.icon.add.onLight
                    },
                    highlighted: {
                        tintColor: Color.primary.lighter,
                        asset: asset.icon.add.onLight
                    },
                    focused: {
                        tintColor: Color.primary.base,
                        asset: asset.icon.add.onLight
                    },
                    selected: {
                        tintColor: Color.primary.darker,
                        asset: asset.icon.add.onLight
                    },
                    disabled: {
                        tintColor: Color.light.darker,
                        asset: asset.icon.add.onLight
                    }
                },
                destructive: {
                    normal: {
                        tintColor: Color.system.red.base,
                        asset: asset.icon.add.onLight
                    },
                    highlighted: {
                        tintColor: Color.system.red.lighter,
                        asset: asset.icon.add.onLight
                    },
                    focused: {
                        tintColor: Color.system.red.base,
                        asset: asset.icon.add.onLight
                    },
                    selected: {
                        tintColor: Color.system.red.darker,
                        asset: asset.icon.add.onLight
                    },
                    disabled: {
                        tintColor: Color.light.darker,
                        asset: asset.icon.add.onLight
                    }
                }
            },
            onDark: {
                regular: {
                    normal: {
                        tintColor: Color.primary.base,
                        asset: asset.icon.add.onLight
                    },
                    highlighted: {
                        tintColor: Color.primary.lighter,
                        asset: asset.icon.add.onLight
                    },
                    focused: {
                        tintColor: Color.primary.base,
                        asset: asset.icon.add.onLight
                    },
                    selected: {
                        tintColor: Color.primary.darker,
                        asset: asset.icon.add.onLight
                    },
                    disabled: {
                        tintColor: Color.light.darker,
                        asset: asset.icon.add.onLight
                    }
                },
                destructive: {
                    normal: {
                        tintColor: Color.system.red.base,
                        asset: asset.icon.add.onLight
                    },
                    highlighted: {
                        tintColor: Color.system.red.lighter,
                        asset: asset.icon.add.onLight
                    },
                    focused: {
                        tintColor: Color.system.red.base,
                        asset: asset.icon.add.onLight
                    },
                    selected: {
                        tintColor: Color.system.red.darker,
                        asset: asset.icon.add.onLight
                    },
                    disabled: {
                        tintColor: Color.light.darker,
                        asset: asset.icon.add.onLight
                    }
                }
            }
        },
        primary: {
            onLight: {
                regular: {
                    normal: {
                        label: label.body.center.onDark,
                        background: Color.primary.base,
                        cornerRadius: Radius.medium
                    },
                    highlighted: {
                        label: label.body.center.onDark,
                        background: Color.primary.lighter,
                        cornerRadius: Radius.medium
                    },
                    focused: {
                        label: label.body.center.onDark,
                        background: Color.primary.base,
                        cornerRadius: Radius.medium
                    },
                    selected: {
                        label: label.body.center.onDark,
                        background: Color.primary.darker,
                        cornerRadius: Radius.medium
                    },
                    disabled: {
                        label: label.body.center.onLight,
                        background: Color.light.darker,
                        cornerRadius: Radius.medium
                    }
                },
                destructive: {
                    normal: {
                        label: label.body.center.onDark,
                        background: Color.system.red.base,
                        cornerRadius: Radius.medium
                    },
                    highlighted: {
                        label: label.body.center.onDark,
                        background: Color.system.red.lighter,
                        cornerRadius: Radius.medium
                    },
                    focused: {
                        label: label.body.center.onDark,
                        background: Color.system.red.base,
                        cornerRadius: Radius.medium
                    },
                    selected: {
                        label: label.body.center.onDark,
                        background: Color.system.red.darker,
                        cornerRadius: Radius.medium
                    },
                    disabled: {
                        label: label.body.center.onLight,
                        background: Color.light.darker,
                        cornerRadius: Radius.medium
                    }
                }
            },
            onDark: {
                regular: {
                    normal: {
                        label: label.body.center.onLight,
                        background: Color.light.base,
                        cornerRadius: Radius.medium
                    },
                    highlighted: {
                        label: label.body.center.onLight,
                        background: Color.light.lighter,
                        cornerRadius: Radius.medium
                    },
                    focused: {
                        label: label.body.center.onLight,
                        background: Color.light.base,
                        cornerRadius: Radius.medium
                    },
                    selected: {
                        label: label.body.center.onLight,
                        background: Color.light.darker,
                        cornerRadius: Radius.medium
                    },
                    disabled: {
                        label: label.body.center.onLight,
                        background: Color.light.darker,
                        cornerRadius: Radius.medium
                    }
                },
                destructive: {
                    normal: {
                        label: label.body.center.onDark,
                        background: Color.system.red.base,
                        cornerRadius: Radius.medium
                    },
                    highlighted: {
                        label: label.body.center.onDark,
                        background: Color.system.red.lighter,
                        cornerRadius: Radius.medium
                    },
                    focused: {
                        label: label.body.center.onDark,
                        background: Color.system.red.base,
                        cornerRadius: Radius.medium
                    },
                    selected: {
                        label: label.body.center.onDark,
                        background: Color.system.red.darker,
                        cornerRadius: Radius.medium
                    },
                    disabled: {
                        label: label.body.center.onLight,
                        background: Color.light.darker,
                        cornerRadius: Radius.medium
                    }
                }
            }
        },
        secondary: {
            onLight: {
                regular: {
                    normal: {
                        label: label.body.center.onDark,
                        background: Color.dark.lighter,
                        cornerRadius: Radius.medium
                    },
                    highlighted: {
                        label: label.body.center.onDark,
                        background: Color.dark.base,
                        cornerRadius: Radius.medium
                    },
                    focused: {
                        label: label.body.center.onDark,
                        background: Color.dark.base,
                        cornerRadius: Radius.medium
                    },
                    selected: {
                        label: label.body.center.onDark,
                        background: Color.dark.lighter,
                        cornerRadius: Radius.medium
                    },
                    disabled: {
                        label: label.body.center.onLight,
                        background: Color.light.darker,
                        cornerRadius: Radius.medium
                    }
                },
                destructive: {
                    normal: {
                        label: label.body.center.onDark,
                        background: Color.system.red.lighter,
                        cornerRadius: Radius.medium
                    },
                    highlighted: {
                        label: label.body.center.onDark,
                        background: Color.system.red.base,
                        cornerRadius: Radius.medium
                    },
                    focused: {
                        label: label.body.center.onDark,
                        background: Color.system.red.lighter,
                        cornerRadius: Radius.medium
                    },
                    selected: {
                        label: label.body.center.onDark,
                        background: Color.system.red.darker,
                        cornerRadius: Radius.medium
                    },
                    disabled: {
                        label: label.body.center.onLight,
                        background: Color.light.darker,
                        cornerRadius: Radius.medium
                    }
                }
            },
            onDark: {
                regular: {
                    normal: {
                        label: label.body.center.onDark,
                        background: Color.dark.lighter,
                        cornerRadius: Radius.medium
                    },
                    highlighted: {
                        label: label.body.center.onDark,
                        background: Color.dark.base,
                        cornerRadius: Radius.medium
                    },
                    focused: {
                        label: label.body.center.onDark,
                        background: Color.dark.base,
                        cornerRadius: Radius.medium
                    },
                    selected: {
                        label: label.body.center.onDark,
                        background: Color.dark.lighter,
                        cornerRadius: Radius.medium
                    },
                    disabled: {
                        label: label.body.center.onLight,
                        background: Color.light.darker,
                        cornerRadius: Radius.medium
                    }
                },
                destructive: {
                    normal: {
                        label: label.body.center.onDark,
                        background: Color.system.red.lighter,
                        cornerRadius: Radius.medium
                    },
                    highlighted: {
                        label: label.body.center.onDark,
                        background: Color.system.red.base,
                        cornerRadius: Radius.medium
                    },
                    focused: {
                        label: label.body.center.onDark,
                        background: Color.system.red.lighter,
                        cornerRadius: Radius.medium
                    },
                    selected: {
                        label: label.body.center.onDark,
                        background: Color.system.red.darker,
                        cornerRadius: Radius.medium
                    },
                    disabled: {
                        label: label.body.center.onLight,
                        background: Color.light.darker,
                        cornerRadius: Radius.medium
                    }
                }
            }
        },
        tertiary: {
            onLight: {
                regular: {
                    normal: {
                        label: label.body.center.onLight,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    },
                    highlighted: {
                        label: label.body.center.onLight,
                        background: Color.primary.lighter,
                        cornerRadius: Radius.medium
                    },
                    focused: {
                        label: label.body.center.onLight,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    },
                    selected: {
                        label: label.body.center.onLight,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    },
                    disabled: {
                        label: label.body.center.lightDarker,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    }
                },
                destructive: {
                    normal: {
                        label: label.body.center.systemRed,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    },
                    highlighted: {
                        label: label.body.center.systemRed,
                        background: Color.primary.lighter,
                        cornerRadius: Radius.medium
                    },
                    focused: {
                        label: label.body.center.systemRed,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    },
                    selected: {
                        label: label.body.center.systemRed,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    },
                    disabled: {
                        label: label.body.center.lightDarker,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    }
                }
            },
            onDark: {
                regular: {
                    normal: {
                        label: label.body.center.onDark,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    },
                    highlighted: {
                        label: label.body.center.onDark,
                        background: Color.primary.lighter,
                        cornerRadius: Radius.medium
                    },
                    focused: {
                        label: label.body.center.onDark,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    },
                    selected: {
                        label: label.body.center.onDark,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    },
                    disabled: {
                        label: label.body.center.darkLighter,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    }
                },
                destructive: {
                    normal: {
                        label: label.body.center.systemRed,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    },
                    highlighted: {
                        label: label.body.center.systemRed,
                        background: Color.primary.lighter,
                        cornerRadius: Radius.medium
                    },
                    focused: {
                        label: label.body.center.systemRed,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    },
                    selected: {
                        label: label.body.center.systemRed,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    },
                    disabled: {
                        label: label.body.center.darkLighter,
                        background: Color.system.clear.base,
                        cornerRadius: Radius.medium
                    }
                }
            }
        }
    }
};

Component.prototype.navigationBar = function (label, separator) {
    return {
        light: {
            background: Color.light.base,
            titleLabel: label.headline.center.onLight,
            titleImage: "None",
            tintColor: Color.primary.base,
            isTranslucent: false,
            separatorBottom: separator.thin.onDark
        },
        dark: {
            background: Color.dark.base,
            titleLabel: label.headline.center.onDark,
            titleImage: "None",
            tintColor: Color.secondary.base,
            isTranslucent: false,
            separatorBottom: separator.thin.onLight
        },
        brandedLight: {
            background: Color.light.base,
            titleLabel: label.headline.center.onLight,
            titleImage: Asset.other.clientLogo.onLight,
            tintColor: Color.primary.base,
            isTranslucent: false,
            separatorBottom: separator.thin.onDark
        }
    }
};

Component.prototype.toolbar = function (separator) {
    return {
        light: {
            separatorTop: separator.thin.onDark,
            separatorBottom: separator.thin.onDark,
            background: Color.light.darker
        },
        dark: {
            separatorTop: separator.thin.onLight,
            separatorBottom: separator.thin.onLight,
            background: Color.dark.lighter
        }
    }
};

Component.prototype.searchBar = function (label, separator, button, asset) {
    return {
        light: {
            height: Unit.septuple,
            separatorTop: separator.thin.onLight,
            separatorBottom: separator.thin.onLight,
            background: Color.light.base,
            asset: asset.icon.search.onLight,
            colorPlaceholder: Color.dark.lighter,
            labelInput: label.body.left.onLight,
            backgroundInput: Color.dark.lighter,
            icon: button.icon.onLight
        },
        dark: {
            height: Unit.septuple,
            separatorTop: separator.thin.onDark,
            separatorBottom: separator.thin.onDark,
            background: Color.dark.base,
            asset: asset.icon.search.onDark,
            colorPlaceholder: Color.light.darker,
            labelInput: label.body.left.onDark,
            backgroundInput: Color.dark.lighter,
            icon: button.icon.onDark
        },
    }
};

Component.prototype.price = function (label) {
    return {
        regular: {
            label: label.body.left.onLight,
            labelFontFamily: FontFamily.price
        },
        discounted: {
            label: label.body.left.primary,
            labelFontFamily: FontFamily.price
        },
        previous: {
            label: label.body.onLight,
            labelFontFamily: FontFamily.price,
            labelDecoration: FontDecoration.Strikethrough,
            labelTransparency: Opacity.highLower
        }
    }
};

Component.prototype.pageControl = {
    currentPageTintColor: Color.primary.base,
    pageTintColor: Color.dark.lighter
};

Component.prototype.rangeSlider = function (label) {
    return {
        light: {
            normal: {
                thumbColor: Color.primary.base,
                trackColor: Color.dark.lighter,
                trackValueColor: Color.primary.darker,
                trackHeight: Unit.base,
                priceLabel: label.body.center.onLight
            },
            highlighted: {
                thumbColor: Color.primary.base,
                trackColor: Color.dark.lighter,
                trackValueColor: Color.primary.darker,
                trackHeight: Unit.base,
                priceLabel: label.body.center.onLight
            },
            focused: {
                thumbColor: Color.primary.base,
                trackColor: Color.dark.lighter,
                trackValueColor: Color.primary.darker,
                trackHeight: Unit.base,
                priceLabel: label.body.center.onLight
            },
            selected: {
                thumbColor: Color.primary.base,
                trackColor: Color.dark.lighter,
                trackValueColor: Color.primary.darker,
                trackHeight: Unit.base,
                priceLabel: label.body.center.onLight
            },
            disabled: {
                thumbColor: Color.primary.lighter,
                trackColor: Color.dark.lighter,
                trackValueColor: Color.primary.darker,
                trackHeight: Unit.base,
                priceLabel: label.body.center.onLight
            }
        },
        dark: {
            normal: {
                thumbColor: Color.primary.base,
                trackColor: Color.light.base,
                trackValueColor: Color.primary.darker,
                trackHeight: Unit.base,
                priceLabel: label.body.center.onLight
            },
            highlighted: {
                thumbColor: Color.primary.base,
                trackColor: Color.light.base,
                trackValueColor: Color.primary.darker,
                trackHeight: Unit.base,
                priceLabel: label.body.center.onLight
            },
            focused: {
                thumbColor: Color.primary.base,
                trackColor: Color.light.base,
                trackValueColor: Color.primary.darker,
                trackHeight: Unit.base,
                priceLabel: label.body.center.onLight
            },
            selected: {
                thumbColor: Color.primary.base,
                trackColor: Color.light.base,
                trackValueColor: Color.primary.darker,
                trackHeight: Unit.base,
                priceLabel: label.body.center.onLight
            },
            disabled: {
                thumbColor: Color.primary.lighter,
                trackColor: Color.light.base,
                trackValueColor: Color.primary.darker,
                trackHeight: Unit.base,
                priceLabel: label.body.center.onLight
            }
        }
    }
};

Component.prototype.segmentedControl = function (label) {
    return {
        light: {
            tintColor: Color.primary.base,
            highlightedLabel: label.footnote.center.onPrimary,
            normalLabel: label.footnote.center.onLight
        }
    }
};

Component.prototype.sortBar =  function (separator) {
    return {
        light: {
            normal: {
                textColor: Color.primary.base,
                background: Color.secondary.lighter,
                separatorBottom: separator.thin.onDark
            },
            highlighted: {
                textColor: Color.primary.base,
                background: Color.secondary.lighter,
                separatorBottom: separator.thin.onDark
            },
            focused: {
                textColor: Color.primary.base,
                background: Color.secondary.lighter,
                separatorBottom: separator.thin.onDark
            },
            selected: {
                textColor: Color.primary.base,
                background: Color.secondary.lighter,
                separatorBottom: separator.thin.onDark
            },
            disabled: {
                textColor: Color.dark.lighter,
                background: Color.secondary.lighter,
                separatorBottom: separator.thin.onDark
            }
        }
    }
};

Component.prototype.separator = {
    none: {
        backgroundOpacity: Opacity.none
    },
    thin: {
        onLight: {
            background: Color.dark.base,
            backgroundOpacity: Opacity.medium,
            height: Unit.sixteenth
        },
        onDark: {
            background: Color.light.base,
            backgroundOpacity: Opacity.medium,
            height: Unit.sixteenth
        }
    },
    medium: {
        onLight: {
            background: Color.dark.base,
            backgroundOpacity: Opacity.medium,
            height: Unit.eighth
        },
        onDark: {
            background: Color.light.base,
            backgroundOpacity: Opacity.medium,
            height: Unit.eighth
        }
    }
};

Component.prototype.promoBanner = function (label) {
    return {
        primary: {
            background: Color.primary.base,
            label: label.subhead.center.onDark
        }
    }
};

Component.prototype.tableCellAccessory = function (button) {
    return {
        Button: button.tertiary.onLight,
        System: {
            None: "none",
            DisclosureIndicator: "AccessoryDetailDisclosureIndicator",
            DisclosureButton: "AccessoryDetailDisclosureButton",
            Checkmark: "checkmark",
            DetailButton: "detailButton"
        }
    }
};

Component.prototype.tableCell = function (label, price, separator, accessory, button) {
    return {
        titleAndDisclosureIndicator: {
            light: {
                normal: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    label: label.body.left.onLight,
                    accessory: accessory.System.DisclosureIndicator
                },
                highlighted: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.primary.lighter,
                    label: label.body.left.onLight,
                    accessory: accessory.System.DisclosureIndicator
                },
                focused: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    label: label.body.left.onLight,
                    accessory: accessory.System.DisclosureIndicator
                },
                selected: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.primary.base,
                    label: label.body.left.onLight,
                    accessory: accessory.System.DisclosureIndicator
                },
                disabled: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    label: label.body.left.onLight,
                    accessory: accessory.System.DisclosureIndicator
                }
            }
        },
        assetTitleAccessory: {
            light: {
                normal: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    label: label.body.left.onLight,
                    accessory: accessory.System.DisclosureIndicator
                },
                highlighted: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.primary.lighter,
                    label: label.body.left.onLight,
                    accessory: accessory.System.DisclosureIndicator
                },
                focused: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    label: label.body.left.onLight,
                    accessory: accessory.System.DisclosureIndicator
                },
                selected: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.primary.base,
                    label: label.body.left.onLight,
                    accessory: accessory.System.DisclosureIndicator
                },
                disabled: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    label: label.body.left.onLight,
                    accessory: accessory.System.DisclosureIndicator
                }
            }
        },
        titleAndButton: {
            light: {
                normal: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    label: label.body.left.onLight,
                    accessory: accessory.Button
                },
                highlighted: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.primary.lighter,
                    label: label.body.left.onLight,
                    accessory: accessory.Button
                },
                focused: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    label: label.body.left.onLight,
                    accessory: accessory.Button
                },
                selected: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.primary.base,
                    label: label.body.left.onLight,
                    accessory: accessory.Button
                },
                disabled: {
                    height: Unit.sextuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    label: label.body.left.onLight,
                    accessory: accessory.Button
                }
            }
        },
        product_01: {
            light: {
                normal: {
                    height: Unit.quinvigintuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    titleLabel: label.body.left.onLight,
                    priceLabel: price.regular,
                    buttonRemove: button.icon.onLight,
                    buttonPrimary: button.primary.onLight
                },
                highlighted: {
                    height: Unit.quinvigintuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    titleLabel: label.body.left.onLight,
                    priceLabel: price.regular,
                    buttonRemove: button.icon.onLight,
                    buttonPrimary: button.primary.onLight
                },
                focused: {
                    height: Unit.quinvigintuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    titleLabel: label.body.left.onLight,
                    priceLabel: price.regular,
                    buttonRemove: button.icon.onLight,
                    buttonPrimary: button.primary.onLight
                },
                selected: {
                    height: Unit.quinvigintuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    titleLabel: label.body.left.onLight,
                    priceLabel: price.regular,
                    buttonRemove: button.icon.onLight,
                    buttonPrimary: button.primary.onLight
                },
                disabled: {
                    height: Unit.quinvigintuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    titleLabel: label.body.left.onLight,
                    priceLabel: price.regular,
                    buttonRemove: button.icon.onLight,
                    buttonPrimary: button.primary.onLight
                },
            }
        },
        product_02: {
            light: {
                normal: {
                    height: Unit.quinvigintuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    titleLabel: label.body.left.onLight,
                    priceLabel: price.regular,
                    quatityLabel: label.footnote.left.onLight,
                    detailLabel: label.footnote.left.onLight
                },
                highlighted: {
                    height: Unit.quinvigintuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    titleLabel: label.body.left.onLight,
                    priceLabel: price.regular,
                    quatityLabel: label.footnote.left.onLight,
                    detailLabel: label.footnote.left.onLight
                },
                focused: {
                    height: Unit.quinvigintuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    titleLabel: label.body.left.onLight,
                    priceLabel: price.regular,
                    quatityLabel: label.footnote.left.onLight,
                    detailLabel: label.footnote.left.onLight
                },
                selected: {
                    height: Unit.quinvigintuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    titleLabel: label.body.left.onLight,
                    priceLabel: price.regular,
                    quatityLabel: label.footnote.left.onLight,
                    detailLabel: label.footnote.left.onLight
                },
                disabled: {
                    height: Unit.quinvigintuple,
                    separatorTop: separator.thin.onLight,
                    separatorBottom: separator.thin.onLight,
                    background: Color.light.base,
                    titleLabel: label.body.left.onLight,
                    priceLabel: price.regular,
                    quatityLabel: label.footnote.left.onLight,
                    detailLabel: label.footnote.left.onLight
                },
            }
        }
    }
};

Component.prototype.collectionCell = function (button, promoBanner, price, label, asset) {
    return {
        product_01: {
            light: {
                normal: {
                    background: Color.light.base,
                    promoBanner: promoBanner.primary,
                    accessoryButton: button.icon.onLight,
                    pricing: price.regular,
                    label: label.caption1.left.onLight,
                    moreColours: asset.other.moreColours.onLight
                },
                highlighted: {
                    background: Color.primary.darker,
                    promoBanner: promoBanner.primary,
                    accessoryButton: button.icon.onLight,
                    pricing: price.regular,
                    label: label.caption1.left.onLight,
                    moreColours: asset.other.moreColours.onLight
                },
                focused: {
                    background: Color.light.darker,
                    promoBanner: promoBanner.primary,
                    accessoryButton: button.icon.onLight,
                    pricing: price.regular,
                    label: label.caption1.left.onLight,
                    moreColours: asset.other.moreColours.onLight
                },
                selected: {
                    background: Color.primary.lighter,
                    promoBanner: promoBanner.primary,
                    accessoryButton: button.icon.onLight,
                    pricing: price.regular,
                    label: label.caption1.left.onLight,
                    moreColours: asset.other.moreColours.onLight
                },
                disabled: {
                    background: Color.light.base,
                    promoBanner: promoBanner.primary,
                    accessoryButton: button.icon.onLight,
                    pricing: price.regular,
                    label: label.caption1.left.onLight,
                    moreColours: asset.other.moreColours.onLight
                }
            }
        }
    }
};

var component = new Component();

var theme = {
    meta: {
        version: 0,
        code: 200
    },
    data: {
        elements: {
            unit: Unit,
            radius: Radius,
            opacity: Opacity,
            colors: Color,
            fontDecoration: FontDecoration,
            fontFamily: FontFamily,
            fontStyle: FontStyle,
            asset: Asset
        },
        components: component,
        screens: {
            PLPFilters: {
                toolbar1: _.defaultsDeep( { "separatorTop": component.separator.none, "separatorBottom": component.separator.medium.onLight }, component.toolbar.light ),
                toolbar2: _.defaultsDeep( { "separatorBottom": component.separator.none, "separatorTop": component.separator.medium.onLight }, component.toolbar.light )
            },
            Wishlist: {
                sectionHeader: _.defaultsDeep( { "height": Unit.quadruple }, component.tableCell.titleAndButton.light.normal )
            },
            Categories: {
                iconExpand: Asset.icon.expand.onLight,
                iconCollapse: Asset.icon.collapse.onLight
            }
        }
    }
};

fs.writeFile( "./theme.json", JSON.stringify( theme, true, 4 ), function ( error ) {
    var message = ( error ) ? error : "File saved."
    // console.log( message );
});
